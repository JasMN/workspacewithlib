import { Component } from '@angular/core';
import { ToggleMenuService } from '../services/toggle-menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private toggleMenuService: ToggleMenuService) {}

  toggleMenu() {
    this.toggleMenuService.setIconClicked(true);
    this.toggleMenuService.toggleMenu();
  }
}
