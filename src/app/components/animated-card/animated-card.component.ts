import { Component, ElementRef, HostListener, Input, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-animated-card',
  templateUrl: './animated-card.component.html',
  styleUrls: ['./animated-card.component.css']
})
export class AnimatedCardComponent {
  @ViewChild('card') card!: ElementRef;
  @Input() data!: any;

  bounds: DOMRect | undefined;
  mouseX = 0;
  mouseY = 0;
  constructor(private renderer: Renderer2) {}

  @HostListener('mouseenter', ['$event'])
  onMouseEnter(event: MouseEvent): void {
    this.bounds = this.card.nativeElement.getBoundingClientRect();
    this.mouseMoveListener = this.renderer.listen(
      'document',
      'mousemove',
      (e: MouseEvent) => this.rotateToMouse(e)
    );
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    if (this.mouseMoveListener) {
      this.mouseMoveListener();
    }
    this.card.nativeElement.style.transform = '';
    this.card.nativeElement.querySelector('.glow').style.backgroundImage = '';
  }

  private mouseMoveListener: Function | undefined;

  private rotateToMouse(e: MouseEvent): void {
    console.log('Rotate function called');
  if (!this.bounds) {
    console.log('Bounds not defined');
    return;
  }
    if (!this.bounds) return;

    const mouseX = e.clientX;
    const mouseY = e.clientY;
    const leftX = mouseX - this.bounds.x;
    const topY = mouseY - this.bounds.y;
    const center = {
      x: leftX - this.bounds.width / 2,
      y: topY - this.bounds.height / 2
    };
    const distance = Math.sqrt(center.x ** 2 + center.y ** 2);

    this.card.nativeElement.style.transform = `
      scale3d(1.01, 1.01, 1.01)
      rotate3d(
        ${center.y / 100},
        ${-center.x / 100},
        0,
        ${Math.log(distance) * 2.5}deg
      )
    `;

    this.card.nativeElement.querySelector('.glow').style.backgroundImage = `
      radial-gradient(
        circle at
        ${center.x * 2 + this.bounds.width / 2}px
        ${center.y * 2 + this.bounds.height / 2}px,
        #ffffffff,
        #0000000d
      )
    `;
  }
}
