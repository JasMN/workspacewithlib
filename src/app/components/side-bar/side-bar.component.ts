import { Component, ElementRef, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';

import { ToggleMenuService } from 'src/app/services/toggle-menu.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'; // Import BreakpointObserver

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent {
  menuOpen = false;
  isClosedClass = false ;
  private menuSubscription: Subscription;
  private iconClicked = false;


  constructor(private toggleMenuService: ToggleMenuService, private elementRef: ElementRef,private breakpointObserver: BreakpointObserver) {
    this.menuSubscription = this.toggleMenuService.menuOpen$.subscribe(
      (isOpen) => {
        this.menuOpen = isOpen;
      }
    );
    this.toggleMenuService.iconClicked$.subscribe((clicked) => {
      this.iconClicked = clicked;
    });
    this.breakpointObserver.observe([Breakpoints.Large]).subscribe(result => {
      if (result.matches) {
        if ( this.menuOpen == true) {
          this.toggleMenuService.toggleMenu();

        }
        // this.menuOpen = false;
        // this.toggleMenuService.setIconClicked(false);
      }
    });

  }


  onMenuItemClick() {
    this.toggleMenuService.toggleMenu();
  }
  toggleMenu() {
    // this.menuOpen = !this.menuOpen;
    this.isClosedClass = !this.isClosedClass;
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: Event) {
    if (!this.elementRef.nativeElement.contains(event.target) && !this.iconClicked) {
      this.toggleMenuService.hideMenu();
      this.iconClicked = false;
    }
    this.iconClicked = false; // Reset the iconClicked flag
  }
  ngOnDestroy() {
    this.menuSubscription.unsubscribe();
  }
}
