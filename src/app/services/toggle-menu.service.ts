import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ToggleMenuService {
  private _menuOpen = new BehaviorSubject<boolean>(false);
  private _iconClicked = new BehaviorSubject<boolean>(false);

  menuOpen$ = this._menuOpen.asObservable();
  iconClicked$ = this._iconClicked.asObservable();

  toggleMenu() {
    this._menuOpen.next(!this._menuOpen.value);
  }
  hideMenu() {
    this._menuOpen.next(false);
  }
  setIconClicked(clicked: boolean) {
    this._iconClicked.next(clicked);
  }
}