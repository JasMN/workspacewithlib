import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { App1SharedModule } from 'projects/app1/src/app/app.module';
import { NavComponent } from './nav/nav.component';
import { TableComponent } from './table/table.component';
import { App1SharedModule } from 'projects/app1/src/app/app.module';
import { App2SharedModule } from 'projects/app2/src/app/app.module';

const routes: Routes = [
  // {path: '', component:NavComponent},
  {path: 'table', component:TableComponent},
  {path: '', loadChildren: () => import('./main-control/main-control.module').then(m => m.MainControlModule)},
  {path: 'app1', loadChildren: () => import('../../projects/app1/src/app/app.module').then(m => m.AppModule)},
  {path: 'app2', loadChildren: () => import('../../projects/app2/src/app/app.module').then(m => m.AppModule)},
  { path: '**', redirectTo: '/app1/one'}
 ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    App1SharedModule.forRoot(),
    App2SharedModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
