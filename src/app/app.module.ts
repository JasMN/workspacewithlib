import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { App1SharedModule } from 'projects/app1/src/app/app.module';
import { NavComponent } from './nav/nav.component';
import { MyLibraryModule } from 'projects/my-library/src/public-api';
import { FormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
import { App2SharedModule } from 'projects/app2/src/app/app.module';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { MainControlModule } from './main-control/main-control.module';
import { HeaderComponent } from './header/header.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { AnimatedCardComponent } from './components/animated-card/animated-card.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TableComponent,
    AdminLayoutComponent,
    AdminDashboardComponent,
    HeaderComponent,
    SideBarComponent,
    AnimatedCardComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MyLibraryModule,
    AppRoutingModule,
    MainControlModule
    // App1SharedModule.forRoot(),
    // App2SharedModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
