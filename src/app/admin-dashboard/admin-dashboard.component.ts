import { Component } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent {
  cards = [
    {
      img: "assets/images/Emtiaz.svg",
      name: "المشروع الأول ",
      des: "شريك خدمات مالية",
      url: "/app1"
    },
    {
      img: "assets/images/منتجات الترميم.svg",
      name: "المشروع الثاني",
      des: "شريك خدمات مالية",
      url: "/app2/one"
    },

  ]
}
