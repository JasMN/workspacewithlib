import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainControlRoutingModule } from './main-control-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainControlRoutingModule
  ]
})
export class MainControlModule { }
