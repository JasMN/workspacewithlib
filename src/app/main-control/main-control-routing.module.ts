import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from '../admin-layout/admin-layout.component';
import { AdminDashboardComponent } from '../admin-dashboard/admin-dashboard.component';
import { ProfileComponent } from '../profile/profile.component';

// const routes: Routes = [];
const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
    //   // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '', component: AdminDashboardComponent },
    //   { path: 'companies', component: CompaniesComponent },
      { path: 'profile', component: ProfileComponent },
    //   { path: 'requests', component: RequestsComponent },
    //   { path: 'services', component: ServicesComponent },
    //   { path: 'clientsRequests', component: ClientsRequestsComponent },
    //   { path: 'manageUsers', component: ManageUsersComponent },
    //   { path: 'partenersRequests', component: PartnersRequestsComponent },
    //   // Add more admin routes here
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainControlRoutingModule { }
