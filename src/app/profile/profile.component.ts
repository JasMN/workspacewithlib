import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  constructor(private http: HttpClient) {}

  // items=[
  //   {
  //     img:"assets/images/profile-user.svg",
  //     name:"عبد الله ناصر المحمدي",
  //     lastMessage:"أهلا بك عبد الله بن ناصر مرحبا بك معانا",
  //     isNew:false
  //   },
  //   {
  //     img:"assets/images/user2.svg",
  //     name:"عبد الله ناصر المحمدي",
  //     lastMessage:"أهلا بك عبد الله بن ناصر مرحبا بك معانا",
  //     isNew:false
  //   },
  //   {
  //     img:"assets/images/profile-user.svg",
  //     name:"عبد الله ناصر المحمدي",
  //     lastMessage:"أهلا بك عبد الله بن ناصر مرحبا بك معانا",
  //     isNew:true
  //   }
  //   ,
  //   {
  //     img:"assets/images/profile-user.svg",
  //     name:"عبد الله ناصر المحمدي",
  //     lastMessage:"أهلا بك عبد الله بن ناصر مرحبا بك معانا",
  //     isNew:false
  //   }
  // ]
  items:any=[]
  ngOnInit() {

    this.http.get('/posts')
      .subscribe(
        (data) => {
          this.items = data
        },
        // (error) => {
        //   console.error('Error:', error);
        // }
      );
  }
}
