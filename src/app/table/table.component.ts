import { Component } from '@angular/core';

@Component({
  selector: 'tablem',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  data=[
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"تمت المراجعة",
     branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"قيد المراجعة",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"قيد المراجعة",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
  ]
}
