import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstComponent } from './pages/first/first.component';
import { SecondComponent } from './pages/second/second.component';

const routes: Routes = [
  { path: 'app2/one', component: FirstComponent },
  { path: 'app2/two', component: SecondComponent },
  // { path: 'app2', redirectTo: 'app2/one' }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
