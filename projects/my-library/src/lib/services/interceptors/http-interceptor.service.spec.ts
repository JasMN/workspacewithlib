import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  private baseUrl = 'https://jsonplaceholder.typicode.com'; // Add your base URL here

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Add common headers, tokens, or modify the request as needed
    const modifiedReq = req.clone({
      url: `${this.baseUrl}/${req.url}`, // Prepend the base URL to the original URL
      setHeaders: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer YOUR_ACCESS_TOKEN',
      },
    });

    // Pass the modified request to the next handler
    return next.handle(modifiedReq).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            console.log('Request was successful:', event);
          }
        },
        (error: HttpErrorResponse) => {
          console.error('Error occurred:', error);
        }
      ),
      catchError((error: any) => {
        // You can perform additional actions or log messages here
        console.error('yasmnees Interceptor caught an error:', error);
        return throwError(error);
      })
    );
  }
}
