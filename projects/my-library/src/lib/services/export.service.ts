// export.service.ts
import { Injectable } from '@angular/core';
// import * as XLSX from 'xlsx'; // For Excel export
import { saveAs } from 'file-saver';
import { Papa } from 'ngx-papaparse';
// declare const XLSX: any;
@Injectable({
  providedIn: 'root'
})
export class ExportService {
  constructor(private papa: Papa) {}

  exportCSV(tableData: any[], fileName: string) {
    const csv = this.papa.unparse(tableData, {
      quotes: false,
      header: true,
    });
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8' });
    saveAs(blob, `${fileName}.csv`);
  }

//   exportExcel(tableData: any[], fileName: string) {
//     const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(tableData);
//     const wb: XLSX.WorkBook = XLSX.utils.book_new();
//     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
//     const blob = new Blob([XLSX.write(wb, { bookType: 'xlsx', type: 'blob' })], {
//       type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//     });
//     saveAs(blob, `${fileName}.xlsx`);
//   }
}
