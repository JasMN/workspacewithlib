import { NgModule } from '@angular/core';
import { MyLibraryComponent } from './my-library.component';
import { CustomButtonComponent } from './custom-button/custom-button.component';
import { TableComponent } from './table/table.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpInterceptorService } from './services/interceptors/http-interceptor.service';



@NgModule({
  declarations: [
    MyLibraryComponent,
    CustomButtonComponent,
    TableComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
     HttpClientModule
  ],
  exports: [
    MyLibraryComponent, CustomButtonComponent, TableComponent
  ],
   providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
  ],
})
export class MyLibraryModule { }
