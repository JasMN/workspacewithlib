import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ExportService } from '../services/export.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnChanges {
  @Input() headers!: Array<string>;
  @Input() data!: Array<any>;
  @Input() withSearch: boolean = false;
  @Input() withControls: boolean = false;
  @Input() withicons: boolean = false;
  get hasHeaderToKeyMapKeys(): boolean {
    return Object.keys(this.headerToKeyMap).length > 0;
  }

  initialForm?: FormGroup;
  @Input() headerToKeyMap: { [key: string]: string } = {};
  searchQuery: string = '';
  filteredDataCopy: any[] = [];
  keys: string[] = [];
  @Output() openModalEvent = new EventEmitter<void>();

  ngOnChanges(changes: SimpleChanges) {
    this.headers = this.headers.map(header => header.trim());
    this.keys = Object.keys(this.data[0]).map(key => key.trim());
    this.trimHeaderToKeyMapValues();

    if (changes['data']) {
      this.updateFilteredDataCopy();
    }
  }
  openModal(data: any) {
    this.openModalEvent.emit(data);
  }
  private trimHeaderToKeyMapValues() {
    const trimmedHeaderToKeyMap: { [key: string]: string } = {};

    for (const key in this.headerToKeyMap) {
      if (this.headerToKeyMap.hasOwnProperty(key)) {
        const trimmedKey = key.trim();
        const trimmedValue = this.headerToKeyMap[key].trim();
        trimmedHeaderToKeyMap[trimmedKey] = trimmedValue;
      }
    }

    this.headerToKeyMap = trimmedHeaderToKeyMap;
  }

  get filteredData() {
    const filtered = this.data.filter((item) =>
      Object.keys(item).some(
        (key) =>
          item[key]
            .toString()
            .toLowerCase()
            .includes(this.searchQuery.toLowerCase())
      )
    );

    this.filteredDataCopy = [...filtered]; // Update filteredDataCopy
    return filtered;
  }

  deleteItem(itemToDelete: any) {
    // Remove the item from the data array
    this.data = this.data.filter(item => item !== itemToDelete);

    // Update filteredDataCopy to reflect the changes
    this.updateFilteredDataCopy();
  }

  search() {
    // Update filteredDataCopy based on the search query
    this.updateFilteredDataCopy();
  }

  constructor(private exportService: ExportService) {}

  exportCSV() {
    this.exportService.exportCSV(this.data, 'table_data');
  }

  private updateFilteredDataCopy() {
    this.filteredDataCopy = [...this.filteredData];
  }
}
