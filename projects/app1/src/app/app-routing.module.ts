import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstComponent } from './pages/first/first.component';
import { SecondComponent } from './pages/second/second.component';

const routes: Routes = [
  // { path: 'app1/one', component: FirstComponent },
  // { path: 'app1/two', component: SecondComponent },
  {path: 'app1/', loadChildren: () => import('./pages/home/home-routing.module').then(m => m.HomeRoutingModule)},
  // {path: 'app1/test', loadChildren: () => import('./pages/test/test.module').then(m => m.TestModule)},
  // { path: 'app1', redirectTo: 'app1/one' }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
