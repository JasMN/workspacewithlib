import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstInTestComponent } from './first-in-test/first-in-test.component';



@NgModule({
  declarations: [
    FirstInTestComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TestModule { }
