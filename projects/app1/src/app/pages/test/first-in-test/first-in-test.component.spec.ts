import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstInTestComponent } from './first-in-test.component';

describe('FirstInTestComponent', () => {
  let component: FirstInTestComponent;
  let fixture: ComponentFixture<FirstInTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstInTestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FirstInTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
