import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../../components/layout/layout.component';
import { FirstComponent } from '../first/first.component';
import { SecondComponent } from '../second/second.component';

const routes: Routes = [
  {
    path: 'app1',
    component: LayoutComponent,
    children: [
      // { path: '', redirectTo: '/app1first', pathMatch: 'full' },
      { path: 'first', component: FirstComponent },
      { path: 'second', component: SecondComponent },
      // { path: 'profile', component: ProfileComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
