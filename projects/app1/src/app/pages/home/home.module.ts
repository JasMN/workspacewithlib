import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { FristComponent } from './frist/frist.component';
import { SecondComponent } from './second/second.component';


@NgModule({
  declarations: [
    FristComponent,
    SecondComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
