import { Component } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent {
  data=[
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"تمت المراجعة",
     branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"قيد المراجعة",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"جمعية جنى",
      date:"15/02/2023",
      status:"قيد المراجعة",
      branch:"فرع وادي الدواسر"
    },
    {
      num:"1028475874",
      name:"مركز رسمة للتصوير الابداعي",
      date:"15/02/2023",
      status:"مكتمل",
      branch:"فرع وادي الدواسر"
    },
  ]
}
