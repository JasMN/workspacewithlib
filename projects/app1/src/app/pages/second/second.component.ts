import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent {
  constructor(private http: HttpClient) {}
data:any=[]
  ngOnInit() {

    this.http.get('/posts')
      .subscribe(
        (data) => {
          this.data = data
        },
        // (error) => {
        //   console.error('Error:', error);
        // }
      );
  }
}
